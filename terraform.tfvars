region              = "us-east-1"
vpc_cidr            = "172.127.0.0/16"
private_subnet_cidr = "172.127.1.0/24"
public_subnets_cidr = ["172.127.101.0/24", "172.127.102.0/24"]