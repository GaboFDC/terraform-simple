variable "region" {
  description = "AWS region to deploy"
}

variable "vpc_cidr" {
  description = "The CIDR block of the vpc"
}

variable "public_subnets_cidr" {
  type        = list
  description = "The CIDR block for the public subnets"
}

variable "private_subnet_cidr" {
  description = "The CIDR block for the private subnet"
}