# terraform-simple

Simple terraform arch for a jenkins job

**_Disclaimer:_** This is a small project intended for local use and not production, some best practices may not be used for ease of usage and setup.

## Configuring

### Use Provided Jenkins

You can use the defined Dockerfile and docker-compose to spin up a jenkins with the job for running the terraform pipeline already configured and working

#### Requirements

- Docker
- docker-compose

#### Steps

1. Clone the repo and enter the directory:

    ```sh
    git clone https://gitlab.com/GaboFDC/terraform-simple.git
    cd terraform-simple
    ```

2. Define or export aws credentials:

    ```sh
    export AWS_ACCESS_KEY_ID=<secret>
    export AWS_SECRET_ACCESS_KEY=<secret>
    ```

3. Run `docker-compose up`
4. Go to `http://localhost:8080`, check and run the job

### Use Own Jenkins

If you want to configure the job on an existing or a different jenkins server use the following steps:

1. Make sure you are already logged on jenkins and have the appropriate permissions.
1. Make sure you have installed and configured all the plugins from `jenkins/plugins.yaml` specially the terraform plugin and AWS credentials.
1. Click "New Item".
1. Enter a proper name for the job, select "Pipeline", and click "OK" Button.
1. . Go to the "Pipeline" section:
   - On definition choose "Pipeline script from SCM"
   - On SCM choose "Git"
   - On "Repository URL" put `https://gitlab.com/GaboFDC/terraform-simple.git`
   - On "Branches to build" use `*/main`
1. Click "Save" button
1. Run the pipeline. Beware of the destroy parameter
